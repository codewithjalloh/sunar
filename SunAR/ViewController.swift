//
//  ViewController.swift
//  SunAR
//
//  Created by wealthyjalloh on 10/09/2017.
//  Copyright © 2017 CWJ. All rights reserved.
//

import UIKit
import SceneKit
import ARKit

class ViewController: UIViewController, ARSCNViewDelegate {

    @IBOutlet var sceneView: ARSCNView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Set the view's delegate
        sceneView.delegate = self
        
        // create sphere
        let sphere = SCNSphere(radius: 0.2)
        
        // to give colour
        let material = SCNMaterial()
        
        
        material.diffuse.contents = UIImage(named: "art.scnassets/sun.jpg")
        
        // give the cube the materail with an array
        sphere.materials = [material]
        
        // create the node
        let node = SCNNode()
        
        // added the position
        node.position = SCNVector3(0, 0.1, -0.5)
        
        // add the note to  the geometry
        node.geometry = sphere
        
        // adding child note to our rootNood sceneView
        sceneView.scene.rootNode.addChildNode(node)
        
        // add some light
        sceneView.automaticallyUpdatesLighting = true
        
       
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        // Create a session configuration
        let configuration = ARWorldTrackingSessionConfiguration()
        
        // Run the view's session
        sceneView.session.run(configuration)
        
        // check to see if Session is supported & World Tracking in supported
        print("Session is supported \(ARSessionConfiguration.isSupported)")
        print("World Tracking in supported \(ARWorldTrackingSessionConfiguration.isSupported)")
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        // Pause the view's session
        sceneView.session.pause()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
    }

    // MARK: - ARSCNViewDelegate
    
/*
    // Override to create and configure nodes for anchors added to the view's session.
    func renderer(_ renderer: SCNSceneRenderer, nodeFor anchor: ARAnchor) -> SCNNode? {
        let node = SCNNode()
     
        return node
    }
*/
    
    func session(_ session: ARSession, didFailWithError error: Error) {
        // Present an error message to the user
        
    }
    
    func sessionWasInterrupted(_ session: ARSession) {
        // Inform the user that the session has been interrupted, for example, by presenting an overlay
        
    }
    
    func sessionInterruptionEnded(_ session: ARSession) {
        // Reset tracking and/or remove existing anchors if consistent tracking is required
        
    }
}
